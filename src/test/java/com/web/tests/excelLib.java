package com.web.tests;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.*;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class excelLib {

	XSSFWorkbook workbook = null;

	public excelLib(String fileName){
		try {

			FileInputStream file = new FileInputStream(new File(fileName));

			//Get the workbook instance for XLS file 
			this.workbook = new XSSFWorkbook(file);


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String[][] readExcelData(){
		//Get first sheet from the workbook
		XSSFSheet sheet = this.workbook.getSheetAt(0);

		int rowNum = sheet.getLastRowNum() + 1;
		int colNum = sheet.getRow(0).getLastCellNum();
		String[][] data = new String[(rowNum - 1)][colNum];

		int k = 0;
		for (int i = 1; i < rowNum; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < colNum; j++) {
				XSSFCell cell = row.getCell(j);
				String value = cell.getStringCellValue();
				data[k][j] = value;
				System.out.println("the value is: " + value);
			}
			k++;
		}
		return data;
	}
}
