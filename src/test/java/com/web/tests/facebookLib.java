package com.web.tests;

import org.openqa.selenium.*;
import java.util.*;

public class facebookLib {

	WebDriver driver;
	
	public  facebookLib(WebDriver driver){
		this.driver = driver;
	}
	
	public void Login(String username, String password) {
		driver.manage().window().maximize();
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(username);
		WebElement pass = driver.findElement(By.id("pass"));
		pass.sendKeys(password);
		pass.sendKeys(Keys.ENTER);
	}
	
	 public void Logout() throws InterruptedException{
		 WebElement account = driver.findElement(By.linkText("Account Settings"));
		 account.click();
		 Thread.sleep(4000);
		 WebElement logout = driver.findElement(By.linkText("Log Out"));
		 logout.click();
	 }
	 
	 
}

