package com.web.tests;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import org.openqa.selenium.*;


public class fbTests {
	
	@DataProvider(name = "loginData")
	public Object[][] getLogins() throws Exception {
		excelLib ex = new excelLib("/Users/rakesh.varma/dev/webTests/src/test/java/com/web/tests/inputs/input.xlsx");
		return ex.readExcelData();
	}
	
	@Test(dataProvider = "loginData")
	public void testLogin(String username, String password) throws InterruptedException{
		WebDriver driver = new FirefoxDriver();
		String baseurl = "http://www.facebook.com";
		driver.get(baseurl);
		facebookLib fb = new facebookLib(driver);
		fb.Login(username, password);
		Assert.assertEquals("Facebook", driver.getTitle());
		fb.Logout();
		driver.close();
	}
}
